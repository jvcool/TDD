from django.conf.urls import url
from .views import index
from .views import message_post, message_table


urlpatterns = [
         url(r'^add_message', message_post, name='add_message'),
        url(r'^$', index, name='index'),
        url(r'^result_table', message_table, name='result_table')
     ]
