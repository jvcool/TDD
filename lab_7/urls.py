from django.conf.urls import url
from .views import index
from .views import add_friend
from .views import delete_friend
from .views import validate_npm
from .views import friend_list
from .views import friend_list_return_models
from .views import friend_description

urlpatterns = [
        url(r'^$', index, name='index'),
        url(r'^add-friend/$', add_friend, name='add-friend'),
        url(r'^validate-npm/$', validate_npm, name='validate-npm'),
        url(r'^delete-friend/(?P<friend_id>[0-9]+)/$', delete_friend, name='delete-friend'),
        url(r'^get-friend-list/$', friend_list, name='get-friend-list') , 
        url(r'^all-friend-list/$', friend_list_return_models, name='all-friend-list'),
        url(r'^friend-description/(?P<friend_id>[0-9]+)/$', friend_description, name='friend-description')
        ]